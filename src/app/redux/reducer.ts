export function user(state = [], action) {
  switch (action.type) {
      case 'ADD_USER':
          return [...state, action.payload];
      default:
          return state;
  }
}

export function cases(state = [], action) {
  switch (action.type) {
      case 'ADD_CASE':
          return [...state, action.payload];
      default:
          return state;
  }
}


export function minKeys(state = [], action) {
  switch (action.type) {
      case 'ADD_MINKEYS':
          return [...state, action.payload];
      default:
          return state;
  }
}

export function keys(state = [], action) {
  switch (action.type) {
      case 'ADD_KEYS':
          return [...state, action.payload];
      default:
          return state;
  }
}
