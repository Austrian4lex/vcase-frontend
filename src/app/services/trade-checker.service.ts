import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import { AlertService } from './alert.service';
import { ApiService } from './api.service';
import { WebsocketService } from './websocket.service';

@Injectable({
  providedIn: 'root'
})
export class TradeCheckerService {

  refreshingOffers: any = [];
  counter: number = 0;

  alertStatus: any = [];

  constructor(
    private _api: ApiService,
    private _alertService: AlertService,
    private _ws: WebsocketService,
    private _store: Store<any>,
  ) {

    let t = this;

    setInterval(() => {
        if (this.counter >= 10) {
            this.counter = 0;

            if (this.refreshingOffers.length > 0) {

                this._api.getOfferStatus(t.refreshingOffers[0]).subscribe(function (data) {
                    switch (data.offerState) {
                        case 0: { return; }
                        case 1: {
                            t.newOfferStatus(data.offerState, t.refreshingOffers[0]);
                            break;
                        }
                        case -1: {
                            return t.newOfferStatus(data.offerState, t.refreshingOffers[0]);
                        }
                        default: break;
                    }
                    switch (data.openingState) {
                        case 0: { return; }
                        case 1:
                        case -1:
                        case -2: {
                            return t.newOpeningState(data.openingState, t.refreshingOffers[0], data.items);
                        }
                        default: break;
                    }
                });
            }
        }
        else
            this.counter += 1;
    }, 1000);

  }


  doTradeCheck(offerId) {
    this.refreshingOffers.push(offerId);
  }


  newOfferStatus(offerState, offerId) {
    let t = this;

    switch (offerState) {
        case 0: { return; }
        case 1: {
            if (!this.alertStatus.includes(offerId)) {

              this.alertStatus.push(offerId);
              // get new KeyAmount
              t.getKeyAmount();
              t._alertService.newAlert('success', 'Das Handelsangebot "' + offerId + '" wurde akzeptiert! Generieren nun deine Gegenstände, gib uns bitte einen Moment :)');
              t._ws.send(JSON.stringify({
                  type: "updateTradeOffer",
                  offerId: offerId,
                  status: "Akzeptiert"
              }));
              t._api.getOfferStatus(offerId).subscribe(data => {
                  t._ws.send(JSON.stringify({
                      type: "saveCaseOpening",
                      offerId: offerId,
                      quantity: data.totalExpectedItems,
                      date: data.time_updated,
                      status: "Generiere",
                      openingLink: '/opening-area/' + offerId,
                      items: data.items
                  }));
              });
            }

            return;
        }
        case -1: {
          t.refreshingOffers.shift();
          t._alertService.newAlert('danger', 'Der Handel wurde abgelehnt');
          t._ws.send(JSON.stringify({
              type: "updateTradeOffer",
              offerId: offerId,
              status: "Abgelehnt"
          }));
          t._ws.send(JSON.stringify({
              type: "saveCaseOpening",
              offerId: offerId,
              status: "Abgelehnt"
          }));
          return;
        }
        default: break;
    }

  }


  newOpeningState(openingState, offerId, items) {
    let t = this;

    switch (openingState) {
        case 0: { return; }
        case 1: {
          t.refreshingOffers.shift();
          t._alertService.newAlert('success', 'Deine Gegenstände vom Handel "' + offerId + '" sind generiert und verfügbar. Viel Glück!');
          t._ws.send(JSON.stringify({
              type: "updateCaseOpeningStatus",
              offerId: offerId,
              items: items,
              status: "Bereit"
          }));

          return;
        }
        case -1: {
          t.refreshingOffers.shift();
          t._alertService.newAlert('danger', 'Deine Kisten konnten nicht erfolgreich generiert werden.');
          t._ws.send(JSON.stringify({
              type: "updateCaseOpeningStatus",
              offerId: offerId,
              status: "Fehler"
          }));

          return;
        }
        case -2: {

          t.refreshingOffers.shift();
          t._alertService.newAlert('danger', 'Einige Kisten konnten nicht erfolgreich generiert werden.');
          t._ws.send(JSON.stringify({
              type: "updateCaseOpeningStatus",
              offerId: offerId,
              items: items,
              status: "Fehler"
          }));

          return;
        }
        default: break;
    }
  }


  getKeyAmount() {

    this._api.getKeyAmount().subscribe(data => {
      this._store.dispatch({
          type: 'ADD_KEYS',
          payload: {
              count: data.keyCount,
          }
      });
    });
  }

}
