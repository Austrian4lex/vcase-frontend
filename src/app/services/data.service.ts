import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private messageSource = new BehaviorSubject<any>(JSON.stringify({ type: "default" }));
  currentMessage = this.messageSource.asObservable();

  constructor() { }

  // Communication btw. WebSocket and case.component
  tradeOffers(offers) {
      this.messageSource.next(JSON.stringify({ type: "tradeOffers", offers: offers }));
  }

  // Communication btw. WebSocket and case.component
  listCaseOpening(cases) {
      this.messageSource.next(JSON.stringify({ type: "cases", cases: cases }));
  }

  recentUnbox(unbox) {
    this.messageSource.next(JSON.stringify({ type: "recentUnbox", unbox: unbox }));
  }

  casesToOpen(cases, isOwner) {
      this.messageSource.next(JSON.stringify({ type: "casesToOpen", isOwner: isOwner, cases: cases }));
  }

  sharedSpinnerItems(offerId, spinner_items, caseImage) {
      this.messageSource.next(JSON.stringify({ type: "shared_spinner_items", offerId: offerId, spinner_items: spinner_items, caseImage: caseImage }));
  }

  startSharedSpin(offerId, spinner_items, i, a) {
      this.messageSource.next(JSON.stringify({ type: "sharedSpin", offerId: offerId, spinner_items: spinner_items, i: i, a: a }));
  }

}
