/************ DEFINE PACKAGE AND STUFF ************/

// Stuff we need
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class AlertService {

  id: number = 0;
  private alerts = new Subject<any>();

  // Function to get all Alerts from observable
  getAlerts(): Observable<any> {
    return this.alerts.asObservable();
  }

  // Function to create new Alert
  newAlert(type, message) {
    this.alerts.next({'id': this.id++, 'type': type, 'message': message});
  }

}
