import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {

  private websocket: any;

  public send(text) {
    let _this = this;
    if (this.websocket && this.websocket.readyState)
      this.websocket.send(JSON.stringify(text));
    else
      setTimeout(function () { _this.send(text); }, 100);
  }

  // Function to connect to websocket server
  public WebsocketConnection (websocketURL): Observable<any> {

      // return messages from server to function
      return Observable.create(observer => {

        // Checks if already connected to websocket
        if (!this.websocket || !this.websocket.readyState) {
            observer.next({ type: "connecting" });
            this.websocket = new WebSocket(websocketURL);
            // Websocket Events
            this.websocket.onopen = function (evt) {
                observer.next({ type: "connected" });
            };
        }

        // Close handler
        this.websocket.onclose = function (evt) {
            observer.next({ type: "close" });
        };

        // Error handler
        this.websocket.onerror = function (evt) {
            observer.next({ type: "error", error: evt });
        };

        this.websocket.onmessage = function (evt) {
            observer.next(JSON.parse(evt.data));
        };

      });

  }

}
