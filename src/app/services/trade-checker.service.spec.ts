import { TestBed, inject } from '@angular/core/testing';

import { TradeCheckerService } from './trade-checker.service';

describe('TradeCheckerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TradeCheckerService]
    });
  });

  it('should be created', inject([TradeCheckerService], (service: TradeCheckerService) => {
    expect(service).toBeTruthy();
  }));
});
