import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private _http: HttpClient
  ) { }

  loginState() {
      return this._http.get<AuthInterface>("/auth/status");
  }

  logout() {
      return this._http.delete("/auth");
  }

  getCases() {
      return this._http.get("/cases");
  }

  getKeyAmount() {
      return this._http.get<KeyInterface>("/keys");
  }

  unbox(caseId, amount) {
      return this._http.post<UnboxInterface>("/cases/" + caseId + "/open", { amount: amount });
  }

  getUserInventory(steamId) {
      return this._http.get("/user_inventory", { params: { steamId: steamId } });
  }

  getOfferStatus(offerId) {
      return this._http.get<OfferInterface>("/offer/" + offerId);
  }

  getMinOpenVolume() {
      return this._http.get("/keys/minopen");
  }

  getItems(skus) {
      return this._http.get<ItemsInterface>("/items", { params: { skus: skus } });
  }

  getWebsocketUrl() {
      return this._http.get<WebsocketInterface>("/websocket");
  }

  setUserAffiliate(waxUserId, affiliatesCode) {
    return this._http.post<UserAffiliate>("/affiliate/save", { waxUserId: waxUserId, affiliatesCode: affiliatesCode });
  }

  getAffiliateStats() {
    return this._http.get<AffiliateStats>("/affiliate/stats");
  }

  setRecruitCode(affiliatesCode) {
    return this._http.post<UserAffiliate>("/affiliate/recruit", { affiliatesCode: affiliatesCode });
  }

}

interface KeyInterface {
  keyCount: any
}

interface ItemsInterface {
  items: any;
}

interface StandardInterface {
  info: any;
}

interface AuthInterface {
  authenticated: boolean;
  username: any,
  avatar: any,
  steamId: any,
  affiliateKey: any
}

interface WebsocketInterface {
  wsurl: string
}

interface UnboxInterface {
  tradeId: number,
  tradeOfferUrl: any,
}

interface OfferInterface {
  totalExpectedItems: number,
  time_created: string,
  time_updated: string
  offerState: number,
  openingState: number,
  items: any
}

interface AffiliateStats {
  affiliatesCode: any,
  affiliatedUser: number,
  affiliatedCases: number,
  affiliatedEarned: number,
  affiliatedByCode: any
}

interface UserAffiliate {
  success: any,
  message: any,
}
