import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { ClipboardModule } from 'ngx-clipboard';

import { user, cases, minKeys, keys } from './redux/reducer'

import { AppComponent } from './app.component';
import { CasesComponent, TradeOfferModal, ItemsInBoxModal } from './components/cases/cases.component';
import { InventoryComponent } from './components/inventory/inventory.component';
import { OpeningAreaComponent } from './components/opening-area/opening-area.component';
import { FaqComponent } from './components/faq/faq.component';
import { TosComponent } from './components/tos/tos.component';
import { NgbdAlert } from './components/alerts/alerts.component';
import { NavigationComponent, AffiliatesModal } from './components/navigation/navigation.component';

import { AlertService } from './services/alert.service';
import { ApiService } from './services/api.service';
import { DataService } from './services/data.service';
import { WebsocketService } from './services/websocket.service';
import { TradeCheckerService } from './services/trade-checker.service';

var appRoutes: Routes = [
    { path: '', component: CasesComponent },
    { path: 'inventar', component: InventoryComponent },
    { path: 'opening-area/:id', component: OpeningAreaComponent },
    { path: 'case', component: CasesComponent },
    { path: 'faq', component: FaqComponent },
    { path: 'tos', component: TosComponent },
    { path: 'auth/login/callback', redirectTo: "/" },
    { path: '**', redirectTo: "/" }
];

@NgModule({
  declarations: [
    AppComponent,
    CasesComponent,
    InventoryComponent,
    OpeningAreaComponent,
    FaqComponent,
    TosComponent,
    NgbdAlert,
    NavigationComponent,
    TradeOfferModal,
    ItemsInBoxModal,
    AffiliatesModal
  ],
  imports: [
    BrowserModule,
    RouterModule,
    FormsModule,
    HttpClientModule,
    NgbModule.forRoot(),
    RouterModule.forRoot(appRoutes),
    StoreModule.forRoot({user, cases, minKeys, keys}),
    ClipboardModule
  ],
  entryComponents: [TradeOfferModal, ItemsInBoxModal, AffiliatesModal],
  providers: [AlertService, ApiService, WebsocketService, DataService, TradeCheckerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
