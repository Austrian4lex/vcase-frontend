/************ DEFINE PACKAGE AND STUFF ************/

// Stuff we need
import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

// Service Imports
import { AlertService } from '../../services/alert.service'


/************ ALERT COMPONENT ************/

@Component({
  selector: 'ngbd-alert',
  templateUrl: './alerts.component.html',
})

export class NgbdAlert implements OnDestroy {

  alerts: Array<IAlert> = [];
  subscription: Subscription;

  ngOnDestroy() {
      // unsubscribe to ensure no memory leaks
      this.subscription.unsubscribe();
  }

  constructor(private alertService: AlertService) {
    this.subscription = this.alertService.getAlerts()
      .subscribe(Alerts => { this.alerts.push(Alerts); })
  }

  public closeAlert(alert: IAlert) {
    const index: number = this.alerts.indexOf(alert);
    this.alerts.splice(index, 1);
  }

}

export interface IAlert {
  id: number;
  type: string;
  message: string;
}
