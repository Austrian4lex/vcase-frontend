import { Component, ElementRef, Input } from '@angular/core';
import { RouterLink } from '@angular/router';
import { DomSanitizer} from '@angular/platform-browser'
import { Store } from '@ngrx/store';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { AlertService } from '../../services/alert.service';
import { ApiService } from '../../services/api.service';
import { DataService } from '../../services/data.service';
import { WebsocketService } from '../../services/websocket.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent {

  waitFor(ms) { return new Promise((r) => { return setTimeout(r, ms) }) }

  authenticated: boolean = false;
  username: any;
  avatar: any;
  steamId: any;

  usersOnline: number = 0;
  totalCasesOpened: number = 0;
  skinsValue: number = 0

  chat_input: any;
  messages: any = [];

  keyCount: number = 0;

  progression: number = 0;
  level: number = 1;

  recentUnboxings: any = [];


  constructor(
    private _api: ApiService,
    private _ws: WebsocketService,
    private alertService: AlertService,
    private dataExchanger: DataService,
    private _store: Store<any>,
    private el: ElementRef,
    private _sanitizer: DomSanitizer,
    private _modalService: NgbModal,
  ) {

    this.loginState();
    this.getRecentUnboxings();
    this.getUserLevel();

    // websocket initialization
    this._api.getWebsocketUrl().subscribe((url) => {
        this._ws.WebsocketConnection(url.wsurl).subscribe((data) => {
            this.handleWebSocket(data);
        });
    });

    this._store.select('keys').subscribe(keys => {
      if (keys.length > 0)
          this.keyCount = keys[keys.length-1].count;
    });

  }


  auth() {
    window.location.href = "/auth";
  }

  handleWebSocket(data) {
    switch (data.type) {
        case 'close':
        case 'error':
            alert("Bitte Seite neu laden! Danke :)");
            break;
        case 'alert': {
            this.alertService.newAlert(data.alert, data.message);
            break;
        }
        case 'recentMessages': {
            for (let i = 0; i < data.recentMessages.length; i++) {
                this.messages.push(data.recentMessages[i]);
                this.chatScroll();
            }
            this.chatScroll();
            break;
        }
        case 'stats': {
          this.usersOnline = data.usersOnline;
          this.totalCasesOpened = data.totalCasesOpened;
          this.skinsValue = data.skinsValue;
          break;
        }
        case 'chat': {
            this.messages.push(data);
            this.chatScroll();
            break;
        }
        case 'userLevel': {
            this.level = data.level;
            this.progression = data.progression;
            break;
        }
        case 'recentUnboxings': {
            this.recentUnboxings = data.recentUnboxings;
            break;
        }
        case 'recentUnbox': {
            this.dataExchanger.recentUnbox(data.unboxed);
            this.recentUnboxings.unshift(data.unboxed);
            break;
        }


        case 'casesOpen': {
            this.dataExchanger.listCaseOpening(data.cases);
            break;
        }
        case 'tradeOffers': {
            this.dataExchanger.tradeOffers(data.offers);
            break;
        }
        case 'casesToOpen': {
            this.dataExchanger.casesToOpen(data.cases, data.isOwner);
            break;
        }
        case 'shared_spinner_items': {
            this.dataExchanger.sharedSpinnerItems(data.offerId, data.spinner_items, data.caseImage);
        }
        case 'startSharedSpin': {
            this.dataExchanger.startSharedSpin(data.offerId, data.spinner_items, data.i, data.a);
        }
    }
  }


  loginState() {
    this._api.loginState().subscribe(data => {
        if (data.authenticated) {
            this._store.dispatch({
                type: 'ADD_USER',
                payload: {
                    authenticated: data.authenticated,
                    username: data.username,
                    avatar: data.avatar,
                    steamId: data.steamId,
                }
            });
            this.authenticated = data.authenticated;
            this.username = data.username;
            this.avatar = data.avatar;
            this.steamId = data.steamId;
            this.getKeyAmount();
        }
        else {
            this.authenticated = data.authenticated;
        }
    });
  }


  logout() {
    this._api.logout().subscribe(function (data) {
        if (data)
          window.location.reload();
    });
  }


  // get keyamout
  getKeyAmount() {

    if (!this.authenticated)
      return;

    this._api.getKeyAmount().subscribe(data => {
      this.keyCount = data.keyCount;

      this._store.dispatch({
          type: 'ADD_KEYS',
          payload: {
              count: parseInt(data.keyCount)
          }
      });
    });
  }


  async chatScroll() {
    let scrollPane;
    // Always scroll to bottom automatically
    scrollPane = this.el.nativeElement.querySelector('.chat-content');
    await this.waitFor(10); //fix for not scrolling correct
    scrollPane.scrollTop = (scrollPane.scrollHeight);
  }

  sendToChat(message) {
    if (this.chat_input == "" || this.chat_input == null || !this.authenticated)
        return;

    this._ws.send(JSON.stringify({
        type: "chat",
        message: message
    }));

    this.chat_input = "";
  }

  // get progression width
  getProgression() {
      return (this.progression * 25) + "%";
  }


  convertToHex(color) {
      color = color.replace("#", "");
      return this._sanitizer.bypassSecurityTrustStyle("6px solid rgba(" + parseInt(color.substring(0, 2), 16) + "," + parseInt(color.substring(2, 4), 16) + "," + parseInt(color.substring(4, 6), 16) + ", 0.6)");
  }

  getRecentUnboxings() {
      this._ws.send(JSON.stringify({ type: "getRecentUnboxings" }));
  }

  getUserLevel () {
      this._ws.send(JSON.stringify({ type: "getUserLevel" }));
  }

  openAffiliate() {
    var modalRef = this._modalService.open(AffiliatesModal);
  }


}


@Component({
  selector: 'affiliates-modal',
  templateUrl: './affiliates.html'
})
export class AffiliatesModal {

  loading: boolean = true;

  affiliateKey: any;
  affiliatedUser: number;
  affiliatedCases: number;
  affiliatedEarned: number;
  affiliatedByCode: any;

  constructor(
    public activeModal: NgbActiveModal,
    private alertService: AlertService,
    private _store: Store<any>,
    private _api: ApiService,
  ) {

    this.getAffiliatesStats();
  }

  getAffiliatesStats() {

    this.loading = true;

    this._api.getAffiliateStats().subscribe(data => {
      this.affiliateKey = data.affiliatesCode;
      this.affiliatedUser = data.affiliatedUser;
      this.affiliatedCases = data.affiliatedCases;
      this.affiliatedEarned = data.affiliatedEarned;
      this.affiliatedByCode = data.affiliatedByCode;
      setTimeout(() => {
        this.loading = false;
      }, 400);
    })
  }


  saveAffiliatesCode(waxUserId, affiliatesCode) {
    this.activeModal.close('Close click');

    if (this.affiliateKey)
      return this.alertService.newAlert("danger", "Du besitzt bereits einen Affiliate Code: " +this.affiliateKey)

    if (!waxUserId || !affiliatesCode)
      return this.alertService.newAlert("danger", "Überprüfe bitte deine Angaben")

    if (isNaN(waxUserId))
      return this.alertService.newAlert("danger", "Die WAX User ID darf nur aus Zahlen bestehen");

    this._api.setUserAffiliate(waxUserId, affiliatesCode).subscribe(data => {
      if (data.success) {
        this.alertService.newAlert("success", data.message);
      } else {
        this.alertService.newAlert("danger", data.message);
      }
    }, error => {
      this.alertService.newAlert("danger", error.statusText);
    });
  }


  saveRecruitCode(affiliatesCode) {
    if (this.affiliatedByCode)
      return this.alertService.newAlert("info", "Du wurdest bereits vom Code: " +this.affiliatedByCode + " rekrutiert");

    this._api.setRecruitCode(affiliatesCode).subscribe(data => {
      if (data.success) {
        this.getAffiliatesStats();
      } else {
        this.activeModal.close('Close click');
        this.alertService.newAlert("danger", data.message);
     }
    });
  }

}
