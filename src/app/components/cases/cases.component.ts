import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { DomSanitizer} from '@angular/platform-browser'
import { RouterLink } from '@angular/router';
import { Store } from '@ngrx/store';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { AlertService } from '../../services/alert.service';
import { ApiService } from '../../services/api.service';
import { DataService } from '../../services/data.service';
import { WebsocketService } from '../../services/websocket.service';
import { TradeCheckerService } from '../../services/trade-checker.service';


@Component({
  selector: 'app-cases',
  templateUrl: './cases.component.html',
  styleUrls: ['./cases.component.css']
})
export class CasesComponent implements OnInit {

  waitFor(ms) { return new Promise((r) => { return setTimeout(r, ms) }) }

  counter: number = 0;
  authenticated: boolean = false;
  hasKeys: boolean = false;

  didTradeStatusPopUp: boolean;
  didCaseStatusPopUp: boolean;

  refreshingOffers: any = [];

  cases: any = [];
  casesLoading: boolean = true;

  offers: any = [];
  listCaseOpening: any = [];

  minkeys: number = 1;
  keyCount: number = 0;

  constructor(
    private _api: ApiService,
    private el: ElementRef,
    private _ws: WebsocketService,
    private alertService: AlertService,
    private dataExchanger: DataService,
    private _store: Store<any>,
    private _modalService: NgbModal,
    private _tradeChecker: TradeCheckerService
  ) { }

  ngOnInit() {

    this.getMinOpenVolume();
    this.getCases();

    this._ws.send(JSON.stringify({ type: 'getListCaseOpening' }));

    this._store.select('user').subscribe(user => {
        if (user.length > 0) {
          this.authenticated = user[user.length-1].authenticated;
        }
    });

    this._store.select('cases').subscribe(cases => {
        if (cases.length > 0)
            this.cases = cases[cases.length-1];
    });

    this._store.select('minKeys').subscribe(minKeys => {
        if (minKeys.length > 0)
            this.minkeys = minKeys[minKeys.length-1].count;
    });

    this._store.select('keys').subscribe(keys => {
      if (keys.length > 0)
          this.keyCount = keys[keys.length-1].count;
    });


    this.dataExchanger.currentMessage.subscribe(info => {
        var newInfo = JSON.parse(info);
        switch (newInfo.type) {
            case 'cases': {
                if (newInfo.cases)
                    this.listCaseOpening = newInfo.cases.reverse();
                else
                    this.listCaseOpening = [];
                break;
            }
            case 'tradeOffers': {
                if (newInfo.offers)
                    this.offers = newInfo.offers.reverse();
                else
                    this.offers = [];
                break;
            }
            default: break;
        }
    });

  }

  // function to forward to /auth
  auth() { window.location.href = "/auth"; };


  // get min open volume
  getMinOpenVolume () {
    if (!this.minkeys) {
        this._api.getMinOpenVolume().subscribe(function (data) {
            this._store.dispatch({
                type: 'ADD_MINKEYS',
                payload: data
            });
        });
    }

    return;
  }

  // get cases schema to display them on site
  getCases() {
      this.casesLoading = true;
      if (this.cases.length <= 0)
          this._api.getCases().subscribe(data => {
              this._store.dispatch({
                  type: 'ADD_CASE',
                  payload: data
              });
              this.casesLoading = false;
          });
      else
          return;
  }

  // function for caseopening init
  initCaseOpening(id, keys, caseName) {

      if (this.keyCount < keys)
          return this.alertService.newAlert('danger', 'Du hast nicht genügend vKeys um den Vorgang abzuschließen');

      if (this.minkeys > keys)
          return this.alertService.newAlert('warning', 'Die Mindestanzahl an Kisten die geöffnet werden müssen, beträgt/betragen ' + this.minkeys);

      this._api.unbox(id, keys).subscribe(data => {
          if (data.tradeId) {
              this.didTradeStatusPopUp = false;
              this.didCaseStatusPopUp = false;
              var modalRef = this._modalService.open(TradeOfferModal, { keyboard: false, backdrop: 'static' });
              modalRef.componentInstance.tradeOfferUrl = data.tradeOfferUrl;
              modalRef.componentInstance.offerId = data.tradeId;
              var tempTradeID_1 = data.tradeId;
              this._api.getOfferStatus(data.tradeId).subscribe(data => {
                  this._ws.send(JSON.stringify({
                      type: "saveTradeOffer",
                      offers: {
                          id: tempTradeID_1,
                          caseName: caseName,
                          totalExpectedItems: data.totalExpectedItems,
                          time_created: data.time_created,
                          state_name: "Ausstehend",
                          offerURL: "https://trade.opskins.com/trade-offers#offer_" + tempTradeID_1,
                      }
                  }));
              });
          }
          else {
              console.log(data);
          }
      }, function (error) { return this.alertService.newAlert('danger', 'Server: ' + error.error.message); });
  }

  // Open Modal to show what's inside case
  itemsInside(kase) {
    let _this = this;
      var sku = JSON.stringify(kase);
      sku = sku.substring(1, sku.length - 1);
      var modalRef = _this._modalService.open(ItemsInBoxModal, { size: 'lg', centered: true });
      this._api.getItems(sku).subscribe(function (data) {
          data.items.sort(sortHighToLow);
          function sortHighToLow(a, b) {
              if (a["suggested_price"] === b["suggested_price"])
                  return 0;
              else
                  return (a["suggested_price"] < b["suggested_price"]) ? 1 : -1;
          }
          modalRef.componentInstance.items = data.items;
      });
  }

  // get all Offers from cases opened on this site
  getUserOffers() {
      if (!this.authenticated)
          return;
      this._ws.send(JSON.stringify({ type: "getTradeOffers" }));
  }

  // gets list of cases to open / have been opened
  getUserCasesToOpen() {
      if (!this.authenticated)
          return;
      this._ws.send(JSON.stringify({ type: "getListCaseOpening" }));
  }

  // check if there are any trade offers (for tabs, icon+text)
  checkTradeOffers() {
      if (!this.authenticated)
          return;
      if (this.offers.length > 0)
          return true;
      return false;
  }

  // check if there are any cases to open (for tabs, icon+text)
  checkCasesToOpen() {
      if (!this.authenticated)
          return;
      if (this.listCaseOpening.length > 0)
          return true;
      return false;
  }

  // (MANUELL) Check offer status when clicked on it (from table)
  checkOfferStatus(offerId) {
      var OFFERID = offerId;
      var offerStatus = this.el.nativeElement.querySelector("#offer_" + OFFERID);

      if (offerStatus.textContent != "Ausstehend")
          return;

      let t = this

      this._api.getOfferStatus(OFFERID).subscribe(function (data) {
          switch (data.offerState) {
              case 0: {
                  return;
              }
              case 1: {
                  return t._tradeChecker.newOfferStatus(data.offerState, OFFERID);
              }
              case -1: {
                  return t._tradeChecker.newOfferStatus(data.offerState, OFFERID);
              }
              default: break;
          }
      });
  }

  // (MANUELL) checks opening status of case by tradeID when clicked on it
  checkOpenStatus(offerId) {
      var OFFERID = offerId;
      var offerStatus = this.el.nativeElement.querySelector("#status_" + OFFERID);

      if (offerStatus.textContent != "Generiere")
          return;

      let t = this

      this._api.getOfferStatus(OFFERID).subscribe(function (data) {
          switch (data.openingState) {
              case 0: {
                  return;
              }
              case 1: {
                  t._tradeChecker.newOpeningState(data.openingState, OFFERID, data.items);
                  break;
              }
              case -1:
              case -2: {
                  t._tradeChecker.newOpeningState(data.openingState, OFFERID, data.items);
                  break;
              }
              default: break;
          }
      });
  }


}




@Component({
  selector: 'cases-modal',
  templateUrl: './cases-modal.html'
})
export class TradeOfferModal {

  @Input() tradeOfferUrl;
  @Input() offerId;

  constructor(
    public activeModal: NgbActiveModal,
    private _dataExchanger: DataService,
    private _tradeChecker: TradeCheckerService,
    private alertService: AlertService,
  ) { }

  checkOfferStatus(offerId) {
    this.activeModal.close('Close click');
    this._tradeChecker.doTradeCheck(offerId);
  }

  cancelOffer(offerId) {
    this.activeModal.close('Close click');
    this.alertService.newAlert('warning', 'Um den Handel zu beenden, drücke bitte "DECLINE" auf der WAX Trade Seite');
  }

}


@Component({
  selector: 'items-in-modal',
  templateUrl: './items-in-modal.html'
})
export class ItemsInBoxModal {

  @Input() items;

  constructor(
    public activeModal: NgbActiveModal,
    private _sanitizer: DomSanitizer,
  ) { }

  convertToHex(color) {
    color = color.replace("#", "");
    return this._sanitizer.bypassSecurityTrustStyle("2px solid rgba(" + parseInt(color.substring(0, 2), 16) + "," + parseInt(color.substring(2, 4), 16) + "," + parseInt(color.substring(4, 6), 16) + ", 0.6)");
  }

}
