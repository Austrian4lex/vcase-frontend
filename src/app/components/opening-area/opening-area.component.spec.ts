import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpeningAreaComponent } from './opening-area.component';

describe('OpeningAreaComponent', () => {
  let component: OpeningAreaComponent;
  let fixture: ComponentFixture<OpeningAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpeningAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpeningAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
