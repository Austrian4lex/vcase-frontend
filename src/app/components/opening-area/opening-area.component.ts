import { Component, OnInit, ElementRef } from '@angular/core';
import { DomSanitizer} from '@angular/platform-browser'
import { ActivatedRoute } from '@angular/router';

import { AlertService } from '../../services/alert.service';
import { ApiService } from '../../services/api.service';
import { DataService } from '../../services/data.service';
import { WebsocketService } from '../../services/websocket.service';

@Component({
  selector: 'app-opening-area',
  templateUrl: './opening-area.component.html',
  styleUrls: ['./opening-area.component.css']
})
export class OpeningAreaComponent implements OnInit {


  casesToOpen:any = [];
  hasCasesToOpen:boolean = false;
  isOwner:boolean = false;
  spinner_items:any = [];
  spinner_item_minimum:number = 200;
  hidden_colors:any = ["#FFD700", "#eb4b4b"];
  isSpinning:boolean = false;
  firstTime:boolean = true;

  offerId: any;
  caseImage: any;

  url_share:any = location.href;

  isCopied: boolean;

  constructor(
    private route: ActivatedRoute,
    private dataService: DataService,
    private ws: WebsocketService,
    private el: ElementRef,
    private api: ApiService,
    private _sanitizer: DomSanitizer,
    private alertService: AlertService
  ) { }


  ngOnInit() {
      var _this = this;

      this.route.paramMap.subscribe(function (params) {
          _this.offerId = params.get('id');
          _this.ws.send(JSON.stringify({
              type: "getCasesToOpen",
              offerId: _this.offerId
          }));
      });

      this.dataService.currentMessage.subscribe(info => {

          var newInfo = JSON.parse(info);

          if (newInfo.type == "recentUnbox") {
              _this.casesToOpen.items.forEach(function (item) {
                  if (newInfo.unbox.caseId == item.caseId)
                      item.opened = true;
              });
          }

          if (newInfo.type == "casesToOpen" && _this.firstTime) {


              _this.isOwner = newInfo.isOwner;

              if (_this.isOwner && newInfo.cases) {

                  _this.firstTime = false;
                  _this.casesToOpen = newInfo.cases;

                  _this.api.getCases().subscribe(function (data) {
                      var temp = [];
                      temp.push(data);
                      for (var i = 0; i < temp[0].length; i++) {
                          if (data[i].name == newInfo.cases.caseName) {

                              _this.caseImage = data[i].image['300px'];

                              var sku = JSON.stringify(data[i].skus);
                              sku = sku.substring(1, sku.length - 1);

                              _this.api.getItems(sku).subscribe(function (data) {
                                  data.items.forEach(function (item) {
                                      if (_this.hidden_colors.indexOf(item.color) < 0)
                                          _this.spinner_items.push(item);
                                  });

                                  _this.spinner_items.sort(function () {
                                      return 0.5 - Math.random();
                                  });

                                  _this.ws.send(JSON.stringify({
                                      type: "share_spinner_items",
                                      spinner_items: _this.spinner_items,
                                      offerId: _this.offerId,
                                      caseImage: _this.caseImage
                                  }));

                                  _this.hasCasesToOpen = true;
                              });
                          }
                      }
                  });
              } else {
                  if (newInfo.cases) {
                      _this.casesToOpen = newInfo.cases;
                      _this.hasCasesToOpen = true;
                      _this.ws.send(JSON.stringify({
                          "type": "getSharedSpinnerItems",
                          offerId: _this.offerId
                      }));
                  }
              }
          }

          if (newInfo.type == "casesToOpen") {
              _this.casesToOpen = newInfo.cases;
          }

          if ((newInfo.type == "shared_spinner_items") && !_this.isOwner && (_this.offerId == newInfo.offerId)) {
              _this.spinner_items = newInfo.spinner_items;
              _this.caseImage = newInfo.caseImage;
          }

          if ((newInfo.type == "sharedSpin") && !_this.isOwner && (_this.offerId == newInfo.offerId)) {
              _this.spinner_items = newInfo.spinner_items;
              var sp_items_1 = _this.el.nativeElement.querySelector("#spinner-items");
              document.getElementsByClassName("spinner-item");
              sp_items_1.style.transform = "translate(-50px, 0)";
              sp_items_1.style.transition = "none";
              sp_items_1.style.webkitTransition = "none";
              setTimeout(function () {
                  sp_items_1.style.transition = "4s";
                  sp_items_1.style.webkitTransition = "4s";
                  sp_items_1.style.transform = "translate(" + (146 * -(newInfo.i - 4) + 14 - newInfo.a) + "px, 0)";
                  setTimeout(function () {
                      sp_items_1.style.transition = "none";
                      sp_items_1.style.webkitTransition = "none";
                  }, 4000);
              }, 50);
          }
      });
  }

  openCase(caseId) {
      if (this.isSpinning)
          return;

      this.isSpinning = true;

      var sp_items = this.el.nativeElement.querySelector("#spinner-items");
      var spinner_button = this.el.nativeElement.querySelector("#open_" + caseId);

      spinner_button.style.pointerEvents = "none";
      document.getElementsByClassName("spinner-item");
      sp_items.style.transform = "translate(-50px, 0)";
      sp_items.style.transition = "none";
      sp_items.style.webkitTransition = "none";

      var t = this;

      setTimeout(function () {
          for (; t.spinner_items.length < t.spinner_item_minimum;) {
              t.spinner_items = t.spinner_items.concat(t.spinner_items);
          }
          var i = Math.floor(Math.random() * Math.floor(t.spinner_items.length - 60)) + 50;
          var a = Math.floor(Math.random() * Math.floor(143));
          for (var index = 0; index < t.casesToOpen.items.length; index++) {
              if (t.casesToOpen.items[index].caseId == caseId)
                  t.spinner_items[i] = t.casesToOpen.items[index];
          }

          t.ws.send(JSON.stringify({
              type: "startSharedSpin",
              offerId: t.offerId,
              spinner_items: t.spinner_items,
              a: a,
              i: i
          }));

          sp_items.style.transition = "4s";
          sp_items.style.webkitTransition = "4s";
          sp_items.style.transform = "translate(" + (146 * -(i - 4) + 14 - a) + "px, 0)";

          setTimeout(function () {
              spinner_button.style.pointerEvents = "auto";
              sp_items.style.transition = "none";
              sp_items.style.webkitTransition = "none";
              t.isSpinning = false;
          }, 4000);

          setTimeout(function () {
              t.ws.send(JSON.stringify({
                  type: "openCase",
                  offerId: t.offerId,
                  caseId: caseId
              }));
          }, 4000);

      }, 150);
  }


  shareOpening() {

      document.addEventListener('copy', (e) => {
          (<any>window).clipboardData.setData('text/plain', location.href);
          e.preventDefault();
      });

      document.execCommand('copy');
      this.alertService.newAlert('info', 'Der Link wurde in die Zwischenablage kopiert!');
  }

  convertToHex(color) {
      color = color.replace("#", "");
      return this._sanitizer.bypassSecurityTrustStyle("2px solid rgba(" + parseInt(color.substring(0, 2), 16) + "," + parseInt(color.substring(2, 4), 16) + "," + parseInt(color.substring(4, 6), 16) + ", 0.6)");
  }



}


interface Event {
    clipboardData: any;
}
